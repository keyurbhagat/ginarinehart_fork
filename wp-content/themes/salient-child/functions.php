<?php 

add_action( 'wp_enqueue_scripts', 'salient_child_enqueue_styles');
function salient_child_enqueue_styles() {
	
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css', array('font-awesome'));
    
    // enqueue child style
    wp_enqueue_style('child-theme', get_stylesheet_directory_uri() .'/style.css', array('parent-theme'));
    wp_enqueue_style('dhara-style', get_stylesheet_directory_uri() . '/css/dhara-css.css', array());    
    if ( is_rtl() ) 
   		wp_enqueue_style(  'salient-rtl',  get_template_directory_uri(). '/rtl.css', array(), '1', 'screen' );
    
    wp_enqueue_style( 'fancybox-style', get_stylesheet_directory_uri() . '/fancybox/jquery.fancybox.css'); 
    wp_enqueue_script( 'fancybox', get_stylesheet_directory_uri() . '/fancybox/jquery.fancybox.js', array( 'jquery' ) );
}

function gina_widgets_init() {

	register_sidebar( array(
		'name'          => 'Footer form sidebar',
		'id'            => 'footer_form-sidebar',		
                'before_widget' => '<div id="%1$s" class="widget %2$s">','after_widget'  => '</div>', 'before_title'  => '<h4>', 'after_title'   => '</h4>'
	) );

}
add_action( 'widgets_init', 'gina_widgets_init' );



add_shortcode('speeches', 'speeches_shortcode_query');
function speeches_shortcode_query($atts, $content){

    $atts = shortcode_atts(
        array(
            'post_type' => 'post', 
            'post_status' => 'publish',
            'category' => '',
        ), $atts, 'speeches' );


  global $post;

  $args = array(
        'post_type' => $atts['post_type'], 
        'post_status' => $atts['post_status'], 
        'posts_per_page' => 5,
        'tax_query'   => array(
                array(
                    'taxonomy' => 'category', //may be categories, I never remember, try both
                    'field'    => 'slug',
                    'terms'    => $atts['category']
                )
            )
        );

  $out = '';
  $posts = new WP_Query($args);
  $excerpt_length = (!empty($options['blog_excerpt_length'])) ? intval($options['blog_excerpt_length']) : 15; 
  $out .= '<div class="speech-listings">';
    if ($posts->have_posts())
        while ($posts->have_posts()):
            
                $posts->the_post();
                //$speach = get_field('',get_the_ID());
                $out .= '<div class="row"><div class="col span_4">';

                if ( has_post_thumbnail() ) { $out .= get_the_post_thumbnail( get_the_ID() ); }

                $out .= '</div><div class="col span_8 speech-content"><h2 class="title"><a href="'.get_permalink().'" title="' . get_the_title() . '"  class="green">'.get_the_title() .'</a></h2><div class="nbw-separator " style="height: .75rem"></div><p class="excerpt_class">'. nectar_excerpt($excerpt_length*2) .'<a href="'.get_permalink().'" title="' . get_the_title() . '">  Read More</a></p></div>';
                $out .= '</div>';
                $out .= do_shortcode('[divider width="full"]');
                
          
        endwhile;
    else
        return; // no posts found
    $out .= '</div>';
    wp_reset_query();
    return $out;
}


add_shortcode('category-box', 'category_box_shortcode');
function category_box_shortcode($atts, $content){

    $atts = shortcode_atts(
        array(
            'category' => '',
        ), $atts, 'speeches' );

    $args = array('child_of' => $atts['category']);
    $categories = get_categories( $args );
    $out = '';

    $out .= '<div class="category-box"><form id="category-select" class="category-select" action="" method="get"><select name="cate" id="news-categories"><option>Category Name</option>';
    foreach($categories as $category) { 
        if($_GET['cate'] == $category->slug){ 
            $out .= '<option value="'.$category->slug.'" selected="selected" >'.$category->name.'</option>';
        }
        else{
            $out .= '<option value="'.$category->slug.'" >'.$category->name.'</option>';    
        }
        //echo '<p>Category: <a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category->name ) . '" ' . '>' . $category->name.'</a> </p> ';
    }
    $out .= '</select></form></div>';
  
    return $out;
}

function ajax_nectar_excerpt($limit) {

    if(has_excerpt()) {
        $the_excerpt = get_the_excerpt();
        $the_excerpt = preg_replace('/\[[^\]]+\]/', '', $the_excerpt);  # strip shortcodes, keep shortcode content
        return wp_trim_words($the_excerpt, $limit);
    } else {
        $the_content = get_the_content();
        $the_content = preg_replace('/\[[^\]]+\]/', '', $the_content);  # strip shortcodes, keep shortcode content
        return wp_trim_words($the_content, $limit);
    }
}

/*function ajax_load_news_by_category(){
    $cat = $_POST['catSlug'];
    
    echo do_shortcode('[ajax_load_more id="news-post" container_type="div" css_classes="row blog-recent columns-3" post_type="post" category="'.$cat.'" posts_per_page="12" transition="none"]');    
    
    die();
   
}
add_action('wp_ajax_ajax_load_news_by_category', 'ajax_load_news_by_category'); 
add_action('wp_ajax_nopriv_ajax_load_news_by_category', 'ajax_load_news_by_category'); */
?>